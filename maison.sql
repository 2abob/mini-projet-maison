-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Sam 04 Mai 2019 à 13:58
-- Version du serveur :  5.7.11
-- Version de PHP :  7.0.3
--
-- Structure de la table `location`
--

CREATE TABLE `location` (
  `IDLOCATION` varchar(10) NOT NULL,
  `LOCATION` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `location`
--

INSERT INTO `location` (`IDLOCATION`, `LOCATION`) VALUES
('location1', 'campagne'),
('location2', 'ville');

-- --------------------------------------------------------

--
-- Structure de la table `maison`
--

CREATE TABLE `maison` (
  `IDMAISON` varchar(10) NOT NULL,
  `IDTYPE` varchar(10) NOT NULL,
  `IDLOCATION` varchar(10) NOT NULL,
  `SURFACE` int(11) DEFAULT NULL,
  `NOMBRESALLE` int(11) DEFAULT NULL,
  `NOMBREDOUCHE` int(11) DEFAULT NULL,
  `NOMBREWC` int(11) DEFAULT NULL,
  `CLIMATISATION` tinyint(1) DEFAULT NULL,
  `AIRCONDITIONNE` tinyint(1) DEFAULT NULL,
  `IMAGE` varchar(50) DEFAULT NULL,
  `TITLE` varchar(50) DEFAULT NULL,
  `ETAT` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `maison`
--

INSERT INTO `maison` (`IDMAISON`, `IDTYPE`, `IDLOCATION`, `SURFACE`, `NOMBRESALLE`, `NOMBREDOUCHE`, `NOMBREWC`, `CLIMATISATION`, `AIRCONDITIONNE`, `IMAGE`, `TITLE`, `ETAT`) VALUES
('maison22', 'type1', 'location1', 25, 4, 2, 1, 1, 0, 'Sans-titre-20.png', 'appartement campagne 06', 1),
('maison23', 'type2', 'location1', 90, 10, 2, 2, 1, 1, 'Sans-titre-26.png', 'maison campagne 06', 1),
('miason01', 'type1', 'location1', 35, 0, 1, 1, 1, 1, 'appartement-campagne-01.png', 'appartement campagne 01', 1),
('miason02', 'type1', 'location1', 0, 0, 1, 1, 1, 1, 'appartement-campagne-02.png', 'appartement campagne 02', 1),
('miason03', 'type1', 'location1', 37, 0, 1, 1, 0, 0, 'appartement-campagne-03.png', 'appartement campagne 03', 1),
('miason04', 'type1', 'location1', 38, 0, 1, 1, 0, 1, 'appartement-campagne-04.png', 'appartement campagne 04', 1),
('miason05', 'type1', 'location1', 50, 0, 1, 1, 1, 0, 'appartement-campagne-05.png', 'appartement campagne 05', 1),
('miason06', 'type1', 'location2', 20, 0, 1, 1, 0, 1, 'appartement-ville-01.png', 'appartement ville 01', 1),
('miason07', 'type1', 'location2', 30, 0, 1, 1, 0, 0, 'appartement-ville-02.png', 'appartement ville 02', 1),
('miason08', 'type1', 'location2', 25, 0, 1, 1, 0, 0, 'appartement-ville-03.png', 'appartement ville 03', 1),
('miason09', 'type1', 'location2', 33, 0, 1, 1, 1, 1, 'appartement-ville-04.png', 'appartement ville 04', 1),
('miason10', 'type1', 'location2', 27, 0, 1, 1, 1, 1, 'appartement-ville-05.png', 'appartement ville 05', 1),
('miason11', 'type2', 'location1', 80, 0, 2, 2, 0, 0, 'maison-campagne-01.png', 'maison campagne 01', 1),
('miason12', 'type2', 'location1', 95, 0, 2, 2, 0, 1, 'maison-campagne-02.png', 'maison campagne 02', 1),
('miason13', 'type2', 'location1', 75, 0, 2, 2, 1, 0, 'maison-campagne-03.png', 'maison campagne 03', 1),
('miason14', 'type2', 'location1', 90, 0, 1, 1, 1, 0, 'maison-campagne-04.png', 'maison campagne 04', 1),
('miason15', 'type2', 'location1', 78, 0, 1, 1, 0, 0, 'maison-campagne-05.png', 'maison campagne 05', 1),
('miason16', 'type2', 'location2', 74, 0, 1, 1, 1, 1, 'maison-ville-01.png', 'maison ville 01', 1),
('miason17', 'type2', 'location2', 76, 0, 2, 2, 1, 0, 'maison-ville-02.png', 'maison ville 02', 1),
('miason18', 'type2', 'location2', 70, 0, 2, 2, 0, 0, 'maison-ville-03.png', 'maison ville 03', 1),
('miason19', 'type2', 'location2', 80, 0, 1, 1, 0, 0, 'maison-ville-04.png', 'maison ville 04', 1),
('miason20', 'type2', 'location2', 85, 0, 2, 2, 0, 0, 'maison-ville-05.png', 'maison ville 05', 1),
('miason21', 'type2', 'location2', 83, 0, 1, 1, 1, 1, 'maison-ville-06.png', 'maison ville 06', 1);

-- --------------------------------------------------------

--
-- Structure de la table `type`
--

CREATE TABLE `type` (
  `IDTYPE` varchar(10) NOT NULL,
  `TYPE` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `type`
--

INSERT INTO `type` (`IDTYPE`, `TYPE`) VALUES
('type1', 'appartement'),
('type2', 'maison');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `IDUTILISATEUR` varchar(20) NOT NULL,
  `LOGIN` varchar(20) DEFAULT NULL,
  `MDP` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`IDUTILISATEUR`, `LOGIN`, `MDP`) VALUES
('utilisateur1', 'mimmobilier', 'e19d5cd5af0378da05f63f891c7467af');

-- --------------------------------------------------------

--
-- Index pour les tables exportées
--

--
-- Index pour la table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`IDLOCATION`);

--
-- Index pour la table `maison`
--
ALTER TABLE `maison`
  ADD PRIMARY KEY (`IDMAISON`),
  ADD KEY `FK_LOCATIONMAISON` (`IDLOCATION`),
  ADD KEY `FK_TYPEMAISON` (`IDTYPE`);

--
-- Index pour la table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`IDTYPE`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`IDUTILISATEUR`),
  ADD UNIQUE KEY `LOGIN` (`LOGIN`);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `maison`
--
ALTER TABLE `maison`
  ADD CONSTRAINT `FK_LOCATIONMAISON` FOREIGN KEY (`IDLOCATION`) REFERENCES `location` (`IDLOCATION`),
  ADD CONSTRAINT `FK_TYPEMAISON` FOREIGN KEY (`IDTYPE`) REFERENCES `type` (`IDTYPE`);


create view maisonindex as ( select m.IDMAISON, m.ETAT, concat(m.IDTYPE, t.TYPE, m.IDLOCATION, l.LOCATION, m.SURFACE, m.NOMBRESALLE, m.NOMBREDOUCHE, m.NOMBREWC, m.CLIMATISATION, m.AIRCONDITIONNE, m.IMAGE) as MOTINDEX from maison m join location l on m.IDLOCATION = l.IDLOCATION join type t on m.IDTYPE = t.IDTYPE);
